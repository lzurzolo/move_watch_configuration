﻿namespace move_watch_configuration_forms
{
    partial class Generate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNumberOfMovements = new System.Windows.Forms.Label();
            this.txtNumberOfMovements = new System.Windows.Forms.TextBox();
            this.grpRestTime = new System.Windows.Forms.GroupBox();
            this.nudMaximumRestTime = new System.Windows.Forms.NumericUpDown();
            this.nudMinimumRestTime = new System.Windows.Forms.NumericUpDown();
            this.lblMaximumTime = new System.Windows.Forms.Label();
            this.lblMinimumTime = new System.Windows.Forms.Label();
            this.grpMovementOptions = new System.Windows.Forms.GroupBox();
            this.rdoAll = new System.Windows.Forms.RadioButton();
            this.rdoBoth = new System.Windows.Forms.RadioButton();
            this.rdoRight = new System.Windows.Forms.RadioButton();
            this.rdoLeft = new System.Windows.Forms.RadioButton();
            this.grpAngles = new System.Windows.Forms.GroupBox();
            this.nudMaximumAngle = new System.Windows.Forms.NumericUpDown();
            this.nudMinimumAngle = new System.Windows.Forms.NumericUpDown();
            this.lblMaximumAngle = new System.Windows.Forms.Label();
            this.lblMinimumAngle = new System.Windows.Forms.Label();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.grpMirror = new System.Windows.Forms.GroupBox();
            this.rdoMirrorNone = new System.Windows.Forms.RadioButton();
            this.rdoMirrorSome = new System.Windows.Forms.RadioButton();
            this.rdoMirrorAll = new System.Windows.Forms.RadioButton();
            this.grpPreparationTime = new System.Windows.Forms.GroupBox();
            this.nudMaximumPrepTime = new System.Windows.Forms.NumericUpDown();
            this.nudMinimumPrepTime = new System.Windows.Forms.NumericUpDown();
            this.lblMaximumPrep = new System.Windows.Forms.Label();
            this.lblMinimumPrep = new System.Windows.Forms.Label();
            this.grpMoveTime = new System.Windows.Forms.GroupBox();
            this.nudMaximumMovetime = new System.Windows.Forms.NumericUpDown();
            this.nudMinimumMovetime = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.grpRestTime.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaximumRestTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMinimumRestTime)).BeginInit();
            this.grpMovementOptions.SuspendLayout();
            this.grpAngles.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaximumAngle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMinimumAngle)).BeginInit();
            this.grpMirror.SuspendLayout();
            this.grpPreparationTime.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaximumPrepTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMinimumPrepTime)).BeginInit();
            this.grpMoveTime.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaximumMovetime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMinimumMovetime)).BeginInit();
            this.SuspendLayout();
            // 
            // lblNumberOfMovements
            // 
            this.lblNumberOfMovements.AutoSize = true;
            this.lblNumberOfMovements.Location = new System.Drawing.Point(12, 16);
            this.lblNumberOfMovements.Name = "lblNumberOfMovements";
            this.lblNumberOfMovements.Size = new System.Drawing.Size(119, 13);
            this.lblNumberOfMovements.TabIndex = 0;
            this.lblNumberOfMovements.Text = "Number Of Movements:";
            // 
            // txtNumberOfMovements
            // 
            this.txtNumberOfMovements.Location = new System.Drawing.Point(139, 13);
            this.txtNumberOfMovements.Name = "txtNumberOfMovements";
            this.txtNumberOfMovements.Size = new System.Drawing.Size(58, 20);
            this.txtNumberOfMovements.TabIndex = 1;
            this.txtNumberOfMovements.Text = "0";
            this.txtNumberOfMovements.TextChanged += new System.EventHandler(this.txtNumberOfMovements_TextChanged);
            // 
            // grpRestTime
            // 
            this.grpRestTime.Controls.Add(this.nudMaximumRestTime);
            this.grpRestTime.Controls.Add(this.nudMinimumRestTime);
            this.grpRestTime.Controls.Add(this.lblMaximumTime);
            this.grpRestTime.Controls.Add(this.lblMinimumTime);
            this.grpRestTime.Location = new System.Drawing.Point(15, 45);
            this.grpRestTime.Name = "grpRestTime";
            this.grpRestTime.Size = new System.Drawing.Size(134, 77);
            this.grpRestTime.TabIndex = 2;
            this.grpRestTime.TabStop = false;
            this.grpRestTime.Text = "Rest Time:";
            // 
            // nudMaximumRestTime
            // 
            this.nudMaximumRestTime.DecimalPlaces = 1;
            this.nudMaximumRestTime.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nudMaximumRestTime.Location = new System.Drawing.Point(62, 47);
            this.nudMaximumRestTime.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nudMaximumRestTime.Name = "nudMaximumRestTime";
            this.nudMaximumRestTime.ReadOnly = true;
            this.nudMaximumRestTime.Size = new System.Drawing.Size(54, 20);
            this.nudMaximumRestTime.TabIndex = 8;
            this.nudMaximumRestTime.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            // 
            // nudMinimumRestTime
            // 
            this.nudMinimumRestTime.DecimalPlaces = 1;
            this.nudMinimumRestTime.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nudMinimumRestTime.Location = new System.Drawing.Point(62, 21);
            this.nudMinimumRestTime.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nudMinimumRestTime.Name = "nudMinimumRestTime";
            this.nudMinimumRestTime.ReadOnly = true;
            this.nudMinimumRestTime.Size = new System.Drawing.Size(54, 20);
            this.nudMinimumRestTime.TabIndex = 7;
            this.nudMinimumRestTime.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            // 
            // lblMaximumTime
            // 
            this.lblMaximumTime.AutoSize = true;
            this.lblMaximumTime.Location = new System.Drawing.Point(6, 49);
            this.lblMaximumTime.Name = "lblMaximumTime";
            this.lblMaximumTime.Size = new System.Drawing.Size(54, 13);
            this.lblMaximumTime.TabIndex = 1;
            this.lblMaximumTime.Text = "Maximum:";
            // 
            // lblMinimumTime
            // 
            this.lblMinimumTime.AutoSize = true;
            this.lblMinimumTime.Location = new System.Drawing.Point(7, 23);
            this.lblMinimumTime.Name = "lblMinimumTime";
            this.lblMinimumTime.Size = new System.Drawing.Size(51, 13);
            this.lblMinimumTime.TabIndex = 0;
            this.lblMinimumTime.Text = "Minimum:";
            // 
            // grpMovementOptions
            // 
            this.grpMovementOptions.Controls.Add(this.rdoAll);
            this.grpMovementOptions.Controls.Add(this.rdoBoth);
            this.grpMovementOptions.Controls.Add(this.rdoRight);
            this.grpMovementOptions.Controls.Add(this.rdoLeft);
            this.grpMovementOptions.Location = new System.Drawing.Point(15, 134);
            this.grpMovementOptions.Name = "grpMovementOptions";
            this.grpMovementOptions.Size = new System.Drawing.Size(200, 120);
            this.grpMovementOptions.TabIndex = 3;
            this.grpMovementOptions.TabStop = false;
            this.grpMovementOptions.Text = "Movement Options:";
            // 
            // rdoAll
            // 
            this.rdoAll.AutoSize = true;
            this.rdoAll.Location = new System.Drawing.Point(6, 88);
            this.rdoAll.Name = "rdoAll";
            this.rdoAll.Size = new System.Drawing.Size(94, 17);
            this.rdoAll.TabIndex = 3;
            this.rdoAll.Text = "All Movements";
            this.rdoAll.UseVisualStyleBackColor = true;
            this.rdoAll.CheckedChanged += new System.EventHandler(this.rdoOption_CheckedChanged);
            // 
            // rdoBoth
            // 
            this.rdoBoth.AutoSize = true;
            this.rdoBoth.Location = new System.Drawing.Point(6, 65);
            this.rdoBoth.Name = "rdoBoth";
            this.rdoBoth.Size = new System.Drawing.Size(129, 17);
            this.rdoBoth.TabIndex = 2;
            this.rdoBoth.Text = "Only Both Movements";
            this.rdoBoth.UseVisualStyleBackColor = true;
            this.rdoBoth.CheckedChanged += new System.EventHandler(this.rdoOption_CheckedChanged);
            // 
            // rdoRight
            // 
            this.rdoRight.AutoSize = true;
            this.rdoRight.Location = new System.Drawing.Point(6, 42);
            this.rdoRight.Name = "rdoRight";
            this.rdoRight.Size = new System.Drawing.Size(132, 17);
            this.rdoRight.TabIndex = 1;
            this.rdoRight.Text = "Only Right Movements";
            this.rdoRight.UseVisualStyleBackColor = true;
            this.rdoRight.CheckedChanged += new System.EventHandler(this.rdoOption_CheckedChanged);
            // 
            // rdoLeft
            // 
            this.rdoLeft.AutoSize = true;
            this.rdoLeft.Location = new System.Drawing.Point(6, 19);
            this.rdoLeft.Name = "rdoLeft";
            this.rdoLeft.Size = new System.Drawing.Size(125, 17);
            this.rdoLeft.TabIndex = 0;
            this.rdoLeft.Text = "Only Left Movements";
            this.rdoLeft.UseVisualStyleBackColor = true;
            this.rdoLeft.CheckedChanged += new System.EventHandler(this.rdoOption_CheckedChanged);
            // 
            // grpAngles
            // 
            this.grpAngles.Controls.Add(this.nudMaximumAngle);
            this.grpAngles.Controls.Add(this.nudMinimumAngle);
            this.grpAngles.Controls.Add(this.lblMaximumAngle);
            this.grpAngles.Controls.Add(this.lblMinimumAngle);
            this.grpAngles.Location = new System.Drawing.Point(419, 45);
            this.grpAngles.Name = "grpAngles";
            this.grpAngles.Size = new System.Drawing.Size(151, 77);
            this.grpAngles.TabIndex = 4;
            this.grpAngles.TabStop = false;
            this.grpAngles.Text = "Angles:";
            // 
            // nudMaximumAngle
            // 
            this.nudMaximumAngle.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nudMaximumAngle.Location = new System.Drawing.Point(91, 47);
            this.nudMaximumAngle.Maximum = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.nudMaximumAngle.Minimum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.nudMaximumAngle.Name = "nudMaximumAngle";
            this.nudMaximumAngle.ReadOnly = true;
            this.nudMaximumAngle.Size = new System.Drawing.Size(54, 20);
            this.nudMaximumAngle.TabIndex = 9;
            this.nudMaximumAngle.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // nudMinimumAngle
            // 
            this.nudMinimumAngle.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nudMinimumAngle.Location = new System.Drawing.Point(91, 21);
            this.nudMinimumAngle.Maximum = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.nudMinimumAngle.Minimum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.nudMinimumAngle.Name = "nudMinimumAngle";
            this.nudMinimumAngle.ReadOnly = true;
            this.nudMinimumAngle.Size = new System.Drawing.Size(54, 20);
            this.nudMinimumAngle.TabIndex = 9;
            this.nudMinimumAngle.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // lblMaximumAngle
            // 
            this.lblMaximumAngle.AutoSize = true;
            this.lblMaximumAngle.Location = new System.Drawing.Point(6, 49);
            this.lblMaximumAngle.Name = "lblMaximumAngle";
            this.lblMaximumAngle.Size = new System.Drawing.Size(84, 13);
            this.lblMaximumAngle.TabIndex = 1;
            this.lblMaximumAngle.Text = "Maximum Angle:";
            // 
            // lblMinimumAngle
            // 
            this.lblMinimumAngle.AutoSize = true;
            this.lblMinimumAngle.Location = new System.Drawing.Point(6, 23);
            this.lblMinimumAngle.Name = "lblMinimumAngle";
            this.lblMinimumAngle.Size = new System.Drawing.Size(81, 13);
            this.lblMinimumAngle.TabIndex = 0;
            this.lblMinimumAngle.Text = "Minimum Angle:";
            // 
            // btnGenerate
            // 
            this.btnGenerate.Location = new System.Drawing.Point(122, 260);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(75, 23);
            this.btnGenerate.TabIndex = 5;
            this.btnGenerate.Text = "Generate";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(208, 260);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // grpMirror
            // 
            this.grpMirror.Controls.Add(this.rdoMirrorNone);
            this.grpMirror.Controls.Add(this.rdoMirrorSome);
            this.grpMirror.Controls.Add(this.rdoMirrorAll);
            this.grpMirror.Location = new System.Drawing.Point(221, 134);
            this.grpMirror.Name = "grpMirror";
            this.grpMirror.Size = new System.Drawing.Size(158, 96);
            this.grpMirror.TabIndex = 7;
            this.grpMirror.TabStop = false;
            this.grpMirror.Text = "Mirror Options:";
            // 
            // rdoMirrorNone
            // 
            this.rdoMirrorNone.AutoSize = true;
            this.rdoMirrorNone.Location = new System.Drawing.Point(8, 65);
            this.rdoMirrorNone.Name = "rdoMirrorNone";
            this.rdoMirrorNone.Size = new System.Drawing.Size(68, 17);
            this.rdoMirrorNone.TabIndex = 2;
            this.rdoMirrorNone.TabStop = true;
            this.rdoMirrorNone.Text = "No Mirror";
            this.rdoMirrorNone.UseVisualStyleBackColor = true;
            this.rdoMirrorNone.CheckedChanged += new System.EventHandler(this.rdoOptionMirror_CheckChanged);
            // 
            // rdoMirrorSome
            // 
            this.rdoMirrorSome.AutoSize = true;
            this.rdoMirrorSome.Location = new System.Drawing.Point(8, 42);
            this.rdoMirrorSome.Name = "rdoMirrorSome";
            this.rdoMirrorSome.Size = new System.Drawing.Size(81, 17);
            this.rdoMirrorSome.TabIndex = 1;
            this.rdoMirrorSome.TabStop = true;
            this.rdoMirrorSome.Text = "Mirror Some";
            this.rdoMirrorSome.UseVisualStyleBackColor = true;
            this.rdoMirrorSome.CheckedChanged += new System.EventHandler(this.rdoOptionMirror_CheckChanged);
            // 
            // rdoMirrorAll
            // 
            this.rdoMirrorAll.AutoSize = true;
            this.rdoMirrorAll.Location = new System.Drawing.Point(8, 19);
            this.rdoMirrorAll.Name = "rdoMirrorAll";
            this.rdoMirrorAll.Size = new System.Drawing.Size(65, 17);
            this.rdoMirrorAll.TabIndex = 0;
            this.rdoMirrorAll.TabStop = true;
            this.rdoMirrorAll.Text = "Mirror All";
            this.rdoMirrorAll.UseVisualStyleBackColor = true;
            this.rdoMirrorAll.CheckedChanged += new System.EventHandler(this.rdoOptionMirror_CheckChanged);
            // 
            // grpPreparationTime
            // 
            this.grpPreparationTime.Controls.Add(this.nudMaximumPrepTime);
            this.grpPreparationTime.Controls.Add(this.nudMinimumPrepTime);
            this.grpPreparationTime.Controls.Add(this.lblMaximumPrep);
            this.grpPreparationTime.Controls.Add(this.lblMinimumPrep);
            this.grpPreparationTime.Location = new System.Drawing.Point(155, 45);
            this.grpPreparationTime.Name = "grpPreparationTime";
            this.grpPreparationTime.Size = new System.Drawing.Size(126, 77);
            this.grpPreparationTime.TabIndex = 8;
            this.grpPreparationTime.TabStop = false;
            this.grpPreparationTime.Text = "Prep Time:";
            // 
            // nudMaximumPrepTime
            // 
            this.nudMaximumPrepTime.DecimalPlaces = 1;
            this.nudMaximumPrepTime.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nudMaximumPrepTime.Location = new System.Drawing.Point(65, 46);
            this.nudMaximumPrepTime.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nudMaximumPrepTime.Name = "nudMaximumPrepTime";
            this.nudMaximumPrepTime.Size = new System.Drawing.Size(54, 20);
            this.nudMaximumPrepTime.TabIndex = 3;
            this.nudMaximumPrepTime.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            // 
            // nudMinimumPrepTime
            // 
            this.nudMinimumPrepTime.DecimalPlaces = 1;
            this.nudMinimumPrepTime.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nudMinimumPrepTime.Location = new System.Drawing.Point(65, 20);
            this.nudMinimumPrepTime.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nudMinimumPrepTime.Name = "nudMinimumPrepTime";
            this.nudMinimumPrepTime.Size = new System.Drawing.Size(54, 20);
            this.nudMinimumPrepTime.TabIndex = 2;
            this.nudMinimumPrepTime.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            // 
            // lblMaximumPrep
            // 
            this.lblMaximumPrep.AutoSize = true;
            this.lblMaximumPrep.Location = new System.Drawing.Point(7, 49);
            this.lblMaximumPrep.Name = "lblMaximumPrep";
            this.lblMaximumPrep.Size = new System.Drawing.Size(54, 13);
            this.lblMaximumPrep.TabIndex = 1;
            this.lblMaximumPrep.Text = "Maximum:";
            // 
            // lblMinimumPrep
            // 
            this.lblMinimumPrep.AutoSize = true;
            this.lblMinimumPrep.Location = new System.Drawing.Point(7, 23);
            this.lblMinimumPrep.Name = "lblMinimumPrep";
            this.lblMinimumPrep.Size = new System.Drawing.Size(51, 13);
            this.lblMinimumPrep.TabIndex = 0;
            this.lblMinimumPrep.Text = "Minimum:";
            // 
            // grpMoveTime
            // 
            this.grpMoveTime.Controls.Add(this.nudMaximumMovetime);
            this.grpMoveTime.Controls.Add(this.nudMinimumMovetime);
            this.grpMoveTime.Controls.Add(this.label1);
            this.grpMoveTime.Controls.Add(this.label2);
            this.grpMoveTime.Location = new System.Drawing.Point(287, 45);
            this.grpMoveTime.Name = "grpMoveTime";
            this.grpMoveTime.Size = new System.Drawing.Size(126, 77);
            this.grpMoveTime.TabIndex = 9;
            this.grpMoveTime.TabStop = false;
            this.grpMoveTime.Text = "Move Time:";
            // 
            // nudMaximumMovetime
            // 
            this.nudMaximumMovetime.DecimalPlaces = 1;
            this.nudMaximumMovetime.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nudMaximumMovetime.Location = new System.Drawing.Point(65, 46);
            this.nudMaximumMovetime.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nudMaximumMovetime.Name = "nudMaximumMovetime";
            this.nudMaximumMovetime.Size = new System.Drawing.Size(54, 20);
            this.nudMaximumMovetime.TabIndex = 3;
            this.nudMaximumMovetime.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            // 
            // nudMinimumMovetime
            // 
            this.nudMinimumMovetime.DecimalPlaces = 1;
            this.nudMinimumMovetime.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nudMinimumMovetime.Location = new System.Drawing.Point(65, 20);
            this.nudMinimumMovetime.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nudMinimumMovetime.Name = "nudMinimumMovetime";
            this.nudMinimumMovetime.Size = new System.Drawing.Size(54, 20);
            this.nudMinimumMovetime.TabIndex = 2;
            this.nudMinimumMovetime.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Maximum:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Minimum:";
            // 
            // Generate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(579, 294);
            this.Controls.Add(this.grpMoveTime);
            this.Controls.Add(this.grpPreparationTime);
            this.Controls.Add(this.grpMirror);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnGenerate);
            this.Controls.Add(this.grpAngles);
            this.Controls.Add(this.grpMovementOptions);
            this.Controls.Add(this.grpRestTime);
            this.Controls.Add(this.txtNumberOfMovements);
            this.Controls.Add(this.lblNumberOfMovements);
            this.Name = "Generate";
            this.Text = "Generate Configuration";
            this.grpRestTime.ResumeLayout(false);
            this.grpRestTime.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaximumRestTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMinimumRestTime)).EndInit();
            this.grpMovementOptions.ResumeLayout(false);
            this.grpMovementOptions.PerformLayout();
            this.grpAngles.ResumeLayout(false);
            this.grpAngles.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaximumAngle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMinimumAngle)).EndInit();
            this.grpMirror.ResumeLayout(false);
            this.grpMirror.PerformLayout();
            this.grpPreparationTime.ResumeLayout(false);
            this.grpPreparationTime.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaximumPrepTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMinimumPrepTime)).EndInit();
            this.grpMoveTime.ResumeLayout(false);
            this.grpMoveTime.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaximumMovetime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMinimumMovetime)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNumberOfMovements;
        private System.Windows.Forms.TextBox txtNumberOfMovements;
        private System.Windows.Forms.GroupBox grpRestTime;
        private System.Windows.Forms.Label lblMinimumTime;
        private System.Windows.Forms.Label lblMaximumTime;
        private System.Windows.Forms.GroupBox grpMovementOptions;
        private System.Windows.Forms.RadioButton rdoAll;
        private System.Windows.Forms.RadioButton rdoBoth;
        private System.Windows.Forms.RadioButton rdoRight;
        private System.Windows.Forms.RadioButton rdoLeft;
        private System.Windows.Forms.GroupBox grpAngles;
        private System.Windows.Forms.Label lblMinimumAngle;
        private System.Windows.Forms.Label lblMaximumAngle;
        private System.Windows.Forms.Button btnGenerate;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.NumericUpDown nudMinimumRestTime;
        private System.Windows.Forms.NumericUpDown nudMaximumRestTime;
        private System.Windows.Forms.NumericUpDown nudMaximumAngle;
        private System.Windows.Forms.NumericUpDown nudMinimumAngle;
        private System.Windows.Forms.GroupBox grpMirror;
        private System.Windows.Forms.RadioButton rdoMirrorNone;
        private System.Windows.Forms.RadioButton rdoMirrorSome;
        private System.Windows.Forms.RadioButton rdoMirrorAll;
        private System.Windows.Forms.GroupBox grpPreparationTime;
        private System.Windows.Forms.Label lblMinimumPrep;
        private System.Windows.Forms.Label lblMaximumPrep;
        private System.Windows.Forms.NumericUpDown nudMinimumPrepTime;
        private System.Windows.Forms.NumericUpDown nudMaximumPrepTime;
        private System.Windows.Forms.GroupBox grpMoveTime;
        private System.Windows.Forms.NumericUpDown nudMaximumMovetime;
        private System.Windows.Forms.NumericUpDown nudMinimumMovetime;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}