﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Newtonsoft.Json;

// TODO:
// ensure that when "Both" is selected as a hand option, mirror mode cannot be enabled

namespace move_watch_configuration_forms
{
    public partial class Form1 : Form
    {
        public Logger DebugLogger;

        public enum WhichHand
        {
            Left,
            Right,
            Both
        };

        public struct Movement
        {
            public string WhichHand;
            public string LastHand;
            public float Angle;
            public float RestTime;
            public float PrepTime;
            public float MoveTime;
            public bool Mirror;
            public bool LastMirror;
            public bool Observe;
        }

        private BindingList<Movement> movementList;

        public Form1()
        {
            InitializeComponent();
            grdMovements.RowHeadersVisible = false;
            movementList = new BindingList<Movement>();
            DebugLogger = new Logger();
        }

        public void SetupColumns()
        {
            grdMovements.DataSource = null;
            grdMovements.Rows.Clear();
            grdMovements.Columns.Clear();

            DataGridViewComboBoxColumn handColoumn = new DataGridViewComboBoxColumn()
            {
                Name = "Which Hand",
                DataSource = Enum.GetValues(typeof(WhichHand)),
                ValueType = typeof(WhichHand),
            };

            DataGridViewColumn angleColumn = new DataGridViewColumn()
            {
                Name = "Angle",
                CellTemplate = new DataGridViewTextBoxCell()
            };

            DataGridViewColumn restColumn = new DataGridViewColumn()
            {
                Name = "Rest Time",
                CellTemplate = new DataGridViewTextBoxCell()
            };

            DataGridViewColumn prepColumn = new DataGridViewColumn()
            {
                Name = "Prep Time",
                CellTemplate = new DataGridViewTextBoxCell()
            };

            DataGridViewColumn moveColumn = new DataGridViewColumn()
            {
                Name = "Move Time",
                CellTemplate = new DataGridViewTextBoxCell()
            };

            DataGridViewCheckBoxColumn mirrorColumn = new DataGridViewCheckBoxColumn
            {
                Name = "Mirror",
                FalseValue = "0",
                TrueValue = "1"
            };

            DataGridViewCheckBoxColumn observeColumn = new DataGridViewCheckBoxColumn
            {
                Name = "Observe",
                FalseValue = "0",
                TrueValue = "1"
            };

            grdMovements.Columns.Insert(0, handColoumn);
            grdMovements.Columns.Insert(1, angleColumn);
            grdMovements.Columns.Insert(2, restColumn);
            grdMovements.Columns.Insert(3, prepColumn);
            grdMovements.Columns.Insert(4, moveColumn);
            grdMovements.Columns.Insert(5, mirrorColumn);
            grdMovements.Columns.Insert(6, observeColumn);
            grdMovements.ColumnHeadersVisible = true;
        }
        
        private void generateConfigurationFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Generate gen = new Generate();
            gen.ShowDialog();

            GenerateRandomConfig(
                gen.NumberOfMovements, 
                gen.MinimumRestTime, 
                gen.MaximumRestTime,
                gen.MinimumPrepTime,
                gen.MaximumPrepTime,
                gen.MinimumMoveTime,
                gen.MaximumMoveTime,
                gen.MinimumAngle, 
                gen.MaximumAngle, 
                gen.SelectedMovementOption,
                gen.SelectedMirrorOption);
        }

        private void openConfigurationFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetupColumns();

            string savePath = System.AppDomain.CurrentDomain.BaseDirectory + "config";

            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                InitialDirectory = savePath,
                Filter = "JSON Configuration | *.json"
            };

            if(openFileDialog.ShowDialog() == DialogResult.OK)
            {
                string loadedData = File.ReadAllText(openFileDialog.FileName);
                movementList.Clear();
                movementList = JsonConvert.DeserializeObject<BindingList<Movement>>(loadedData);

                foreach(var m in movementList)
                {
                    WhichHand w = (WhichHand)Enum.Parse(typeof(WhichHand), m.WhichHand);
                    grdMovements.Rows.Add(w, m.Angle, m.RestTime, m.PrepTime, m.MoveTime, Convert.ToInt32(m.Mirror), Convert.ToInt32(m.Observe));
                }

                RecalculateTimeAndMovements();

                grdMovements.ReadOnly = false;
                lblCreateOrOpen.Visible = false;
                lblRightClickHint.Visible = true;
                lblNumberOfMovements.Visible = true;
                txtNumberOfMovements.Visible = true;
                lblTime.Visible = true;
                txtTime.Visible = true;
                btnDeleteRow.Visible = true;

                movementList.Clear();
            }

            /*
            grdMovements.DataSource = null;
            grdMovements.ColumnCount = 0;

            string savePath = System.AppDomain.CurrentDomain.BaseDirectory + "config";

            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                InitialDirectory = savePath,
                Filter = "JSON Configuration | *.json"
            };

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                string loadedData = File.ReadAllText(openFileDialog.FileName);
                movementList.Clear();
                movementList = JsonConvert.DeserializeObject<BindingList<Movement>>(loadedData);
                float time = 0.0f;
                int numberOfMovements = 0;
                foreach (var m in movementList)
                {
                    time += m.time;
                    if (m.position == "Move")
                    {
                        numberOfMovements++;
                    }
                }
                txtTime.Text = time.ToString();
                txtNumberOfMovements.Text = numberOfMovements.ToString();
                grdMovements.DataSource = movementList;
                grdMovements.ReadOnly = false;
                lblCreateOrOpen.Visible = false;
                lblRightClickHint.Visible = true;
                lblNumberOfMovements.Visible = true;
                txtNumberOfMovements.Visible = true;
                lblTime.Visible = true;
                txtTime.Visible = true;
                btnDeleteRow.Visible = true;
            }
            */
        }

        private void saveConfigurationFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string savePath = System.AppDomain.CurrentDomain.BaseDirectory + "config";

            SaveFileDialog saveFileDialog = new SaveFileDialog
            {
                InitialDirectory = savePath,
                Filter = "JSON Configuration | *.json"
            };

            DataGridViewRow rLast = null;

            foreach(DataGridViewRow r in grdMovements.Rows)
            {
                grdMovements.EndEdit();

                Movement m = new Movement();
                m.WhichHand = r.Cells[0].Value.ToString();
                m.Angle = float.Parse(r.Cells[1].Value.ToString());
                m.RestTime = float.Parse(r.Cells[2].Value.ToString());
                m.PrepTime = float.Parse(r.Cells[3].Value.ToString());
                m.MoveTime = float.Parse(r.Cells[4].Value.ToString());
                m.Mirror = Convert.ToBoolean(int.Parse(r.Cells[5].Value.ToString()));
                m.Observe = Convert.ToBoolean(int.Parse(r.Cells[6].Value.ToString()));
                if(r.Index == 0)
                {
                    m.LastHand = "";
                }
                else
                {
                    m.LastHand = rLast.Cells[0].Value.ToString();
                    m.LastMirror = Convert.ToBoolean(int.Parse(rLast.Cells[5].Value.ToString()));
                }
                rLast = r;
                movementList.Add(m);
            }

            string json = JsonConvert.SerializeObject(movementList, Formatting.Indented);

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                File.WriteAllText(saveFileDialog.FileName, json);
            }

            movementList.Clear();
        }

        private void createConfigurationFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            grdMovements.DataSource = null;

            SetupColumns();

            lblCreateOrOpen.Visible = false;
            lblRightClickHint.Visible = true;
            lblNumberOfMovements.Visible = true;
            txtNumberOfMovements.Visible = true;
            lblTime.Visible = true;
            txtTime.Visible = true;
            lblSeconds.Visible = true;
            btnDeleteRow.Visible = true;
        }

        private void btnDeleteRow_Click(object sender, EventArgs e)
        {
            var confirmResult = MessageBox.Show("Confirm Deletion", "Delete", MessageBoxButtons.YesNo);

            if(confirmResult == DialogResult.Yes)
            {
                int row = grdMovements.CurrentCell.RowIndex;
                grdMovements.Rows.RemoveAt(row);
            }
        }

        private void grdMovements_RowValidated(object sender, DataGridViewCellEventArgs e)
        {
            RecalculateTimeAndMovements();
        }

        private void grdMovements_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            RecalculateTimeAndMovements();
        }

        private void RecalculateTimeAndMovements()
        {
            int numberOfMovements = grdMovements.Rows.Count;
            float time = 0.0f;

            foreach (DataGridViewRow r in grdMovements.Rows)
            {
                time += float.Parse(r.Cells[2].Value.ToString());
                time += float.Parse(r.Cells[3].Value.ToString());
                time += float.Parse(r.Cells[4].Value.ToString());
            }

            txtTime.Text = time.ToString();
            txtNumberOfMovements.Text = numberOfMovements.ToString();
        }

        private void grdMovements_MouseDown(object sender, MouseEventArgs e)
        {
            switch(e.Button)
            {
                case MouseButtons.Right:
                {
                    conGridMenuStrip.Show(this, new Point(e.X, e.Y));
                }
                break;
            }
        }

        private void grdMovements_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewCell cell = grdMovements.Rows[e.RowIndex].Cells[e.ColumnIndex];

            if(cell.ColumnIndex == 0)
            {
                DataGridViewCell mirrorCell = grdMovements.Rows[e.RowIndex].Cells[5];
                if (cell.Value.ToString() == "Both")
                {
                    mirrorCell.ReadOnly = true;
                    mirrorCell.Style.BackColor = Color.Gray;
                    mirrorCell.Value = "0";
                }
                else
                {
                    mirrorCell.ReadOnly = false;
                    mirrorCell.Style.BackColor = Color.Empty;
                }
            }

            if (cell.ColumnIndex == 1)
            {
                if (!float.TryParse(cell.Value.ToString(), out float result) || result < 10 || result > 90)
                {
                    if (result < 10)
                        cell.Value = 10;
                    else if (result > 90)
                        cell.Value = 90;
                    MessageBox.Show("Angle must be between 10 and 90");
                }
            }

            // rest time column
            if (cell.ColumnIndex == 2)
            {
                if (!float.TryParse(cell.Value.ToString(), out float result) || result < 0)
                {
                    cell.Value = 0;
                    MessageBox.Show("Rest time must be a positive numerical value");
                }
            }            
            
            // prep time column
            if (cell.ColumnIndex == 3)
            {
                if (!float.TryParse(cell.Value.ToString(), out float result) || result < 0)
                {
                    cell.Value = 0;
                    MessageBox.Show("Prep time must be a positive numerical value");
                }
            }
                        
            // move time column
            if (cell.ColumnIndex == 4)
            {
                if (!float.TryParse(cell.Value.ToString(), out float result) || result < 0)
                {
                    cell.Value = 0;
                    MessageBox.Show("Move time must be a positive numerical value");
                }
            }

            // mirror column
            if(cell.ColumnIndex == 5)
            {
                DataGridViewCell observeCell = grdMovements.Rows[e.RowIndex].Cells[6];
                if(cell.Value.ToString() == "0")
                {
                    observeCell.ReadOnly = false;
                    observeCell.Style.BackColor = Color.White;
                }
                else if(cell.Value.ToString() == "1")
                {
                    observeCell.ReadOnly = true;
                    observeCell.Style.BackColor = Color.Gray;
                    observeCell.Value = Convert.ToInt32(false);
                }
            }
        }

        private void quitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void GenerateRandomConfig(
            uint numberOfMovements, 
            double minRestTime, 
            double maxRestTime, 
            double minPrepTime, 
            double maxPrepTime,
            double minMoveTime,
            double maxMovetime,
            int minAngle, 
            int maxAngle, 
            Generate.MovementOptions movementOpts, 
            Generate.MirrorOptions mirrorOpts)
        {
            grdMovements.DataSource = null;
            movementList.Clear();
            SetupColumns();

            string hand = "Left";
            bool randomHand = false;
            if (movementOpts == Generate.MovementOptions.MLEFT)
            {
                hand = "Left";
            }
            else if (movementOpts == Generate.MovementOptions.MRIGHT)
            {
                hand = "Right";
            }
            else if (movementOpts == Generate.MovementOptions.MBOTH)
            {
                hand = "Both";
            }
            else
            {
                randomHand = true;
            }

            Random rTime = new Random();
            Random rHand = new Random();
            Random rAngle = new Random();
            Random rMirror = new Random();
            Random rPrep = new Random();

            int dividedMinAngle = minAngle / 5;
            int dividedMaxAngle = maxAngle / 5 + 1;

            for(int i = 1; i <= numberOfMovements; ++i)
            {
                int angle = rAngle.Next(dividedMinAngle, dividedMaxAngle) * 5;
                double restTime = rTime.NextDouble() * (maxRestTime - minRestTime) + minRestTime;
                double prepTime = rTime.NextDouble() * (maxPrepTime - minPrepTime) + minPrepTime;
                double moveTime = rTime.NextDouble() * (maxMovetime - minMoveTime) + minMoveTime;
                bool mirror = false;

                if(randomHand)
                {
                    int handNum = rHand.Next(0, 3);
                    if(handNum == 0)
                    {
                        hand = "Left";
                    }
                    else if(handNum == 1)
                    {
                        hand = "Right";
                    }
                    else
                    {
                        hand = "Both";
                    }
                }

                if(hand == "Left" || hand == "Right")
                {
                    if(mirrorOpts == Generate.MirrorOptions.MALL)
                    {
                        mirror = true;
                    }
                    else if(mirrorOpts == Generate.MirrorOptions.MSOME)
                    {
                        mirror = Convert.ToBoolean(rMirror.Next(0, 2));
                    }
                }

                float roundedRestTime = (float)Math.Round(restTime, 1);
                float roundedPrepTime = (float)Math.Round(prepTime, 1);
                float roundedMoveTime = (float)Math.Round(moveTime, 1);

                Movement m = new Movement()
                {
                    WhichHand = hand,
                    LastHand = "",
                    Angle = angle,
                    RestTime = roundedRestTime,
                    PrepTime = roundedPrepTime,
                    MoveTime = roundedMoveTime,
                    Mirror = mirror,
                    LastMirror = false,
                    Observe = false
                };

                movementList.Add(m);
            }

            foreach (var m in movementList)
            {
                WhichHand w = (WhichHand)Enum.Parse(typeof(WhichHand), m.WhichHand);
                grdMovements.Rows.Add(w, m.Angle, m.RestTime, m.PrepTime, m.MoveTime, Convert.ToInt32(m.Mirror), Convert.ToInt32(m.Observe));
            }

            foreach (DataGridViewRow r in grdMovements.Rows)
            {
                if(r.Cells[5].Value.ToString() == "1")
                {
                    r.Cells[6].ReadOnly = true;
                    r.Cells[6].Style.BackColor = Color.Gray;
                }
            }

            RecalculateTimeAndMovements();

            movementList.Clear();
            grdMovements.ReadOnly = false;
            lblCreateOrOpen.Visible = false;
            lblRightClickHint.Visible = true;
            lblNumberOfMovements.Visible = true;
            txtNumberOfMovements.Visible = true;
            lblTime.Visible = true;
            txtTime.Visible = true;
            lblSeconds.Visible = true;
            btnDeleteRow.Visible = true;
        }

        private void insertPromptHereToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (grdMovements.CurrentRow != null)
            {
                int row = grdMovements.CurrentRow.Index;
                grdMovements.Rows.Insert(row + 1, WhichHand.Both, 0, 0.0f, 0.0f, 0.0f, 0, 0);
                grdMovements.Rows[row + 1].Cells[5].ReadOnly = true;
                grdMovements.Rows[row + 1].Cells[5].Style.BackColor = Color.Gray;
                RecalculateTimeAndMovements();
            }
        }

        private void addPromptToEndToolStripMenuItem_Click(object sender, EventArgs e)
        {
            grdMovements.Rows.Add(WhichHand.Both, 10, 0.0f, 0.0f, 0.0f, 0, 0);
            grdMovements.Rows[grdMovements.Rows.Count - 1].Cells[5].ReadOnly = true;
            grdMovements.Rows[grdMovements.Rows.Count - 1].Cells[5].Style.BackColor = Color.Gray;
            RecalculateTimeAndMovements();
        }
    }
}
