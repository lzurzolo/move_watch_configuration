﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace move_watch_configuration_forms
{
    public class Logger
    {
        public string Path = "Log.txt";

        [Conditional("DEBUG")]
        public void Log(string message)
        {
            using (StreamWriter writer = new StreamWriter(Path))
            {
                writer.WriteLine(message);
                writer.Close();
            }
        }
    }
}
