﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace move_watch_configuration_forms
{
    public partial class Generate : Form
    {
        public enum MovementOptions
        {
            MLEFT,
            MRIGHT,
            MBOTH,
            MALL
        };

        public enum MirrorOptions
        {
            MALL,
            MSOME,
            MNONE
        };

        public uint NumberOfMovements { get; set; }
        public double MinimumRestTime { get; set; }
        public double MaximumRestTime { get; set; }
        public double MinimumPrepTime { get; set; }
        public double MaximumPrepTime { get; set; }
        public double MinimumMoveTime { get; set; }
        public double MaximumMoveTime { get; set; }
        public int MinimumAngle { get; set; }
        public int MaximumAngle { get; set; }
        public MovementOptions SelectedMovementOption { get; set; }
        public MirrorOptions SelectedMirrorOption { get; set; }

        public Generate()
        {
            InitializeComponent();
        }

        private void txtNumberOfMovements_TextChanged(object sender, EventArgs e)
        {
            if (UInt32.TryParse(txtNumberOfMovements.Text, out uint n))
            {
                NumberOfMovements = n;
            }
            else
            {
                MessageBox.Show("Number Of Movements can only be an integer value.");
                txtNumberOfMovements.Text = "1";
            }
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            if(nudMaximumAngle.Value < nudMinimumAngle.Value)
            {
                MessageBox.Show("Maximum angle must be greater than minimum angle!");
                return;
            }
            else
            {
                MinimumAngle = Decimal.ToInt32(nudMinimumAngle.Value);
                MaximumAngle = Decimal.ToInt32(nudMaximumAngle.Value);
            }

            if(nudMaximumRestTime.Value < nudMinimumRestTime.Value)
            {
                MessageBox.Show("Maximum Rest time must be greater than minimum Rest time!");
                return;
            }
            else
            {
                MinimumRestTime = Decimal.ToDouble(nudMinimumRestTime.Value);
                MaximumRestTime = Decimal.ToDouble(nudMaximumRestTime.Value);
            }

            if(nudMinimumPrepTime.Value > nudMaximumPrepTime.Value)
            {
                MessageBox.Show("Minimum Prep time must be less than Maximum Prep time!");
                return;
            }
            else
            {
                MinimumPrepTime = Decimal.ToDouble(nudMinimumPrepTime.Value);
                MaximumPrepTime = Decimal.ToDouble(nudMaximumPrepTime.Value);
            }

            if(nudMinimumMovetime.Value > nudMaximumMovetime.Value)
            {
                MessageBox.Show("Minimum Move time must be less than Maximum move time!");
            }
            else
            {
                MinimumMoveTime = Decimal.ToDouble(nudMinimumMovetime.Value);
                MaximumMoveTime = Decimal.ToDouble(nudMaximumMovetime.Value);
            }

            NumberOfMovements = UInt32.Parse(txtNumberOfMovements.Text);
            if(NumberOfMovements == 0)
            {
                MessageBox.Show("Number of movements must be greater than 0!");
                return;
            }

            bool isMovementOptionSelected = false;
            foreach(Control control in grpMovementOptions.Controls)
            {
                if(control is RadioButton)
                {
                    RadioButton radio = control as RadioButton;

                    if(radio.Checked)
                    {
                        isMovementOptionSelected = true;
                        break;
                    }
                }
            }

            if(!isMovementOptionSelected)
            {
                MessageBox.Show("You must select a movement option!");
                return;
            }

            bool isMirrorOptionSelected = false;
            foreach(Control control in grpMirror.Controls)
            {
                if(control is RadioButton)
                {
                    RadioButton radio = control as RadioButton;

                    if(radio.Checked)
                    {
                        isMirrorOptionSelected = true;
                        break;
                    }
                }
            }

            if(!isMirrorOptionSelected)
            {
                MessageBox.Show("You must select a mirror option!");
                return;
            }

            Close();
        }

        private void rdoOption_CheckedChanged(object sender, EventArgs e)
        {
            foreach(Control control in grpMovementOptions.Controls)
            {
                if(control is RadioButton)
                {
                    RadioButton radio = control as RadioButton;

                    if(radio.Checked)
                    {
                        switch(radio.Name)
                        {
                            case "rdoLeft":
                                SelectedMovementOption = MovementOptions.MLEFT;
                                break;
                            case "rdoRight":
                                SelectedMovementOption = MovementOptions.MRIGHT;
                                break;
                            case "rdoAll":
                                SelectedMovementOption = MovementOptions.MALL;
                                break;
                            case "rdoBoth":
                                SelectedMovementOption = MovementOptions.MBOTH;
                                break;
                        }
                    }
                }
            }
        }

        private void rdoOptionMirror_CheckChanged(object sender, EventArgs e)
        {
            foreach(Control control in grpMirror.Controls)
            {
                if(control is RadioButton)
                {
                    RadioButton radio = control as RadioButton;

                    if(radio.Checked)
                    {
                        switch(radio.Name)
                        {
                            case "rdoMirrorAll":
                                SelectedMirrorOption = MirrorOptions.MALL;
                                break;
                            case "rdoMirrorSome":
                                SelectedMirrorOption = MirrorOptions.MSOME;
                                break;
                            case "rdoMirrorNone":
                                SelectedMirrorOption = MirrorOptions.MNONE;
                                break;
                        }
                    }
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
