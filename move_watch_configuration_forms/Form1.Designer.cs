﻿namespace move_watch_configuration_forms
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.grdMovements = new System.Windows.Forms.DataGridView();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generateConfigurationFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openConfigurationFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveConfigurationFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createConfigurationFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.txtNumberOfMovements = new System.Windows.Forms.TextBox();
            this.lblNumberOfMovements = new System.Windows.Forms.Label();
            this.lblTime = new System.Windows.Forms.Label();
            this.txtTime = new System.Windows.Forms.TextBox();
            this.lblSeconds = new System.Windows.Forms.Label();
            this.btnDeleteRow = new System.Windows.Forms.Button();
            this.conGridMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.insertPromptHereToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addPromptToEndToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lblCreateOrOpen = new System.Windows.Forms.Label();
            this.lblRightClickHint = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grdMovements)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.conGridMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // grdMovements
            // 
            this.grdMovements.AllowUserToAddRows = false;
            this.grdMovements.AllowUserToResizeColumns = false;
            this.grdMovements.AllowUserToResizeRows = false;
            this.grdMovements.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdMovements.Location = new System.Drawing.Point(12, 54);
            this.grdMovements.MultiSelect = false;
            this.grdMovements.Name = "grdMovements";
            this.grdMovements.Size = new System.Drawing.Size(704, 329);
            this.grdMovements.TabIndex = 0;
            this.grdMovements.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdMovements_CellEndEdit);
            this.grdMovements.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.grdMovements_RowsRemoved);
            this.grdMovements.RowValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdMovements_RowValidated);
            this.grdMovements.MouseDown += new System.Windows.Forms.MouseEventHandler(this.grdMovements_MouseDown);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(727, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.generateConfigurationFileToolStripMenuItem,
            this.openConfigurationFileToolStripMenuItem,
            this.saveConfigurationFileToolStripMenuItem,
            this.createConfigurationFileToolStripMenuItem,
            this.quitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // generateConfigurationFileToolStripMenuItem
            // 
            this.generateConfigurationFileToolStripMenuItem.Name = "generateConfigurationFileToolStripMenuItem";
            this.generateConfigurationFileToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.generateConfigurationFileToolStripMenuItem.Text = "Generate Configuration File";
            this.generateConfigurationFileToolStripMenuItem.Click += new System.EventHandler(this.generateConfigurationFileToolStripMenuItem_Click);
            // 
            // openConfigurationFileToolStripMenuItem
            // 
            this.openConfigurationFileToolStripMenuItem.Name = "openConfigurationFileToolStripMenuItem";
            this.openConfigurationFileToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.openConfigurationFileToolStripMenuItem.Text = "Open Configuration File";
            this.openConfigurationFileToolStripMenuItem.Click += new System.EventHandler(this.openConfigurationFileToolStripMenuItem_Click);
            // 
            // saveConfigurationFileToolStripMenuItem
            // 
            this.saveConfigurationFileToolStripMenuItem.Name = "saveConfigurationFileToolStripMenuItem";
            this.saveConfigurationFileToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.saveConfigurationFileToolStripMenuItem.Text = "Save Configuration File";
            this.saveConfigurationFileToolStripMenuItem.Click += new System.EventHandler(this.saveConfigurationFileToolStripMenuItem_Click);
            // 
            // createConfigurationFileToolStripMenuItem
            // 
            this.createConfigurationFileToolStripMenuItem.Name = "createConfigurationFileToolStripMenuItem";
            this.createConfigurationFileToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.createConfigurationFileToolStripMenuItem.Text = "Create Configuration File";
            this.createConfigurationFileToolStripMenuItem.Click += new System.EventHandler(this.createConfigurationFileToolStripMenuItem_Click);
            // 
            // quitToolStripMenuItem
            // 
            this.quitToolStripMenuItem.Name = "quitToolStripMenuItem";
            this.quitToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.quitToolStripMenuItem.Text = "Quit";
            this.quitToolStripMenuItem.Click += new System.EventHandler(this.quitToolStripMenuItem_Click);
            // 
            // txtNumberOfMovements
            // 
            this.txtNumberOfMovements.Location = new System.Drawing.Point(15, 418);
            this.txtNumberOfMovements.Name = "txtNumberOfMovements";
            this.txtNumberOfMovements.ReadOnly = true;
            this.txtNumberOfMovements.Size = new System.Drawing.Size(53, 20);
            this.txtNumberOfMovements.TabIndex = 2;
            this.txtNumberOfMovements.Visible = false;
            // 
            // lblNumberOfMovements
            // 
            this.lblNumberOfMovements.AutoSize = true;
            this.lblNumberOfMovements.Location = new System.Drawing.Point(12, 399);
            this.lblNumberOfMovements.Name = "lblNumberOfMovements";
            this.lblNumberOfMovements.Size = new System.Drawing.Size(114, 13);
            this.lblNumberOfMovements.TabIndex = 3;
            this.lblNumberOfMovements.Text = "Number of Movements";
            this.lblNumberOfMovements.Visible = false;
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.Location = new System.Drawing.Point(152, 399);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(30, 13);
            this.lblTime.TabIndex = 4;
            this.lblTime.Text = "Time";
            this.lblTime.Visible = false;
            // 
            // txtTime
            // 
            this.txtTime.Location = new System.Drawing.Point(155, 418);
            this.txtTime.Name = "txtTime";
            this.txtTime.ReadOnly = true;
            this.txtTime.Size = new System.Drawing.Size(53, 20);
            this.txtTime.TabIndex = 5;
            this.txtTime.Visible = false;
            // 
            // lblSeconds
            // 
            this.lblSeconds.AutoSize = true;
            this.lblSeconds.Location = new System.Drawing.Point(210, 423);
            this.lblSeconds.Name = "lblSeconds";
            this.lblSeconds.Size = new System.Drawing.Size(12, 13);
            this.lblSeconds.TabIndex = 6;
            this.lblSeconds.Text = "s";
            this.lblSeconds.Visible = false;
            // 
            // btnDeleteRow
            // 
            this.btnDeleteRow.Location = new System.Drawing.Point(641, 389);
            this.btnDeleteRow.Name = "btnDeleteRow";
            this.btnDeleteRow.Size = new System.Drawing.Size(75, 23);
            this.btnDeleteRow.TabIndex = 17;
            this.btnDeleteRow.Text = "Delete Row";
            this.btnDeleteRow.UseVisualStyleBackColor = true;
            this.btnDeleteRow.Visible = false;
            this.btnDeleteRow.Click += new System.EventHandler(this.btnDeleteRow_Click);
            // 
            // conGridMenuStrip
            // 
            this.conGridMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.insertPromptHereToolStripMenuItem,
            this.addPromptToEndToolStripMenuItem});
            this.conGridMenuStrip.Name = "conGridMenuStrip";
            this.conGridMenuStrip.Size = new System.Drawing.Size(179, 48);
            // 
            // insertPromptHereToolStripMenuItem
            // 
            this.insertPromptHereToolStripMenuItem.Name = "insertPromptHereToolStripMenuItem";
            this.insertPromptHereToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.insertPromptHereToolStripMenuItem.Text = "Insert Prompt Here";
            this.insertPromptHereToolStripMenuItem.Click += new System.EventHandler(this.insertPromptHereToolStripMenuItem_Click);
            // 
            // addPromptToEndToolStripMenuItem
            // 
            this.addPromptToEndToolStripMenuItem.Name = "addPromptToEndToolStripMenuItem";
            this.addPromptToEndToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.addPromptToEndToolStripMenuItem.Text = "Add Prompt To End";
            this.addPromptToEndToolStripMenuItem.Click += new System.EventHandler(this.addPromptToEndToolStripMenuItem_Click);
            // 
            // lblCreateOrOpen
            // 
            this.lblCreateOrOpen.AutoSize = true;
            this.lblCreateOrOpen.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.lblCreateOrOpen.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCreateOrOpen.Location = new System.Drawing.Point(276, 212);
            this.lblCreateOrOpen.Name = "lblCreateOrOpen";
            this.lblCreateOrOpen.Size = new System.Drawing.Size(153, 13);
            this.lblCreateOrOpen.TabIndex = 18;
            this.lblCreateOrOpen.Text = "Create or Open a Configuration";
            // 
            // lblRightClickHint
            // 
            this.lblRightClickHint.AutoSize = true;
            this.lblRightClickHint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRightClickHint.Location = new System.Drawing.Point(9, 38);
            this.lblRightClickHint.Name = "lblRightClickHint";
            this.lblRightClickHint.Size = new System.Drawing.Size(142, 13);
            this.lblRightClickHint.TabIndex = 19;
            this.lblRightClickHint.Text = "Right click to add a new row";
            this.lblRightClickHint.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(727, 450);
            this.Controls.Add(this.lblRightClickHint);
            this.Controls.Add(this.lblCreateOrOpen);
            this.Controls.Add(this.btnDeleteRow);
            this.Controls.Add(this.lblSeconds);
            this.Controls.Add(this.txtTime);
            this.Controls.Add(this.lblTime);
            this.Controls.Add(this.lblNumberOfMovements);
            this.Controls.Add(this.txtNumberOfMovements);
            this.Controls.Add(this.grdMovements);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Move Watch Configuration";
            ((System.ComponentModel.ISupportInitialize)(this.grdMovements)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.conGridMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView grdMovements;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openConfigurationFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveConfigurationFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createConfigurationFileToolStripMenuItem;
        private System.Windows.Forms.TextBox txtNumberOfMovements;
        private System.Windows.Forms.Label lblNumberOfMovements;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.TextBox txtTime;
        private System.Windows.Forms.Label lblSeconds;
        private System.Windows.Forms.Button btnDeleteRow;
        private System.Windows.Forms.ContextMenuStrip conGridMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem quitToolStripMenuItem;
        private System.Windows.Forms.Label lblCreateOrOpen;
        private System.Windows.Forms.Label lblRightClickHint;
        private System.Windows.Forms.ToolStripMenuItem generateConfigurationFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem insertPromptHereToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addPromptToEndToolStripMenuItem;
    }
}

